from django.conf.urls import url
from django.urls import path
from .views import daftar_peminjaman, tambah_peminjaman, tambah_peminjaman_database

urlpatterns = [
	url('daftar/', daftar_peminjaman, name='daftar_peminjaman'),
	url('tambah/', tambah_peminjaman, name='tambah_peminjaman'),
	url('tambah_peminjaman_database_tombol/', tambah_peminjaman_database, name='tambah_peminjaman_database'),
]
