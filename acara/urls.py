from django.conf.urls import url
from django.urls import path
from .views import daftar_acara,update_acara,tambah_acara,tambah_acara_database, delete_acara_database,update_acara_database

urlpatterns = [
	url('tambah/', tambah_acara, name='tambah_acara'),
	url('update/', update_acara, name='update_acara'),
	url('daftar/', daftar_acara, name='daftar_acara'),
	url('update_acara_database/', update_acara_database, name='update_acara_database'),
	url('tambah_acara_database/', tambah_acara_database, name='tambah_acara_database'),
	url('delete_acara_database/', delete_acara_database, name='delete_acara_database'),
]
