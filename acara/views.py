from django.shortcuts import render
from django.http import HttpResponseRedirect
import psycopg2

# Create your views here.
response = {}
def tambah_acara(request):
	try:
		connection = psycopg2.connect(
			user = "hvespbbcktcjqh",
			password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
			host = "ec2-54-83-205-27.compute-1.amazonaws.com",
			port = "5432",
			dbname = "d9dmdcj7ao235n"
		)

		cursor = connection.cursor()

		cursor.execute("SELECT id_stasiun,nama FROM STASIUN S")
		response['data_stasiun'] = cursor.fetchall()

		connection.commit()
	except (Exception, psycopg2.DatabaseError) as error:
		print ("Error while creating postgreSQL table", error)

	finally:
		if(connection):
			cursor.close()
			connection.close()
	
	return render(request, 'form_acara.html', response)
	
def update_acara(request):
	return render(request, 'update_acara.html', response)
	
def daftar_acara(request):
	try:
		connection = psycopg2.connect(
			user = "hvespbbcktcjqh",
			password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
			host = "ec2-54-83-205-27.compute-1.amazonaws.com",
			port = "5432",
			dbname = "d9dmdcj7ao235n"
		)

		cursor = connection.cursor()
		cursor.execute("SELECT A.id_acara,judul,deskripsi,tgl_mulai,tgl_akhir,is_free,S.nama FROM ACARA A, ACARA_STASIUN AT, STASIUN S WHERE S.id_stasiun = AT.id_stasiun AND A.id_acara = AT.id_acara")
		response['data'] = cursor.fetchall()

		cursor.execute("SELECT id_stasiun,nama FROM STASIUN S")
		response['data_stasiun'] = cursor.fetchall()

		connection.commit()
	except (Exception, psycopg2.DatabaseError) as error:
		print ("Error while creating postgreSQL table", error)
	finally:
		if(connection):
			cursor.close()
			connection.close()
	return render(request, 'tabel_acara.html', response)

def tambah_acara_database(request):
	if(request.method == 'POST'):
		judul = request.POST.get('judul')
		deskripsi = request.POST.get('deskripsi')
		is_gratis = request.POST.get('is_gratis')
		mulai = request.POST.get('mulai')
		selesai = request.POST.get('selesai')
		stasiun = request.POST.get('stasiun')

		try:
			connection = psycopg2.connect(
				user = "hvespbbcktcjqh",
				password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
				host = "ec2-54-83-205-27.compute-1.amazonaws.com",
				port = "5432",
				dbname = "d9dmdcj7ao235n"
			)

			cursor = connection.cursor()
			cursor.execute("SELECT MAX(id_acara) FROM ACARA")
			nomor_acara = cursor.fetchone()
			
			cursor.execute("INSERT INTO ACARA VALUES (%s, %s, %s, %s, %s, %s)", (int(nomor_acara[0]) + 1, judul, deskripsi, mulai, selesai,is_gratis))
			cursor.execute("INSERT INTO ACARA_STASIUN VALUES (%s, %s)", (stasiun,int(nomor_acara[0]) + 1))
			connection.commit()

		except (Exception, psycopg2.DatabaseError) as error:
			print ("Error while creating postgreSQL table", error)
		
		finally:
			if(connection):
				cursor.close()
				connection.close()

		return HttpResponseRedirect('daftar/')
	else:
		return HttpResponseRedirect('daftar/')
	
def delete_acara_database(request):
	a_id = request.GET.get('id')
	
	try:
		connection = psycopg2.connect(
			user = "hvespbbcktcjqh",
			password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
			host = "ec2-54-83-205-27.compute-1.amazonaws.com",
			port = "5432",
			dbname = "d9dmdcj7ao235n"
		)

		cursor = connection.cursor()
		cursor.execute("DELETE FROM ACARA WHERE id_acara = %s", [a_id])

		connection.commit()
	except (Exception, psycopg2.DatabaseError) as error:
		print ("Error while creating postgreSQL table", error)
	finally:
		if(connection):
			cursor.close()
			connection.close()
	return HttpResponseRedirect('daftar/')

def update_acara_database(request): 

	if(request.method == 'POST'):
		id_acara = request.POST.get('id_acara')
		judul = request.POST.get('judul')
		deskripsi = request.POST.get('deskripsi')
		is_gratis = request.POST.get('is_gratis')
		mulai = request.POST.get('mulai')
		selesai = request.POST.get('selesai')
		stasiun = request.POST.get('stasiun')

		try:
			connection = psycopg2.connect(
				user = "hvespbbcktcjqh",
				password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
				host = "ec2-54-83-205-27.compute-1.amazonaws.com",
				port = "5432",
				dbname = "d9dmdcj7ao235n"
			)

			cursor = connection.cursor()
			
			cursor.execute("UPDATE ACARA SET judul = %s, deskripsi = %s, tgl_mulai = %s, tgl_akhir = %s, is_free = %s WHERE id_acara = %s", (judul, deskripsi, mulai, selesai,is_gratis, id_acara))
			
			connection.commit()

		except (Exception, psycopg2.DatabaseError) as error:
			print ("Error while creating postgreSQL table", error)
		
		finally:
			if(connection):
				cursor.close()
				connection.close()

		return HttpResponseRedirect('daftar/')
	else:
		return HttpResponseRedirect('daftar/')

