from django.conf.urls import url
from django.urls import path
from .views import daftar_sepeda,update_sepeda,tambah_sepeda, tambah_sepeda_database, delete_sepeda_database

urlpatterns = [
	url('tambah/', tambah_sepeda, name='tambah_sepeda'),
	url('update/', update_sepeda, name='update_sepeda'),
	url('daftar/', daftar_sepeda, name='daftar_sepeda'),
	url('tambah_sepeda_database/', tambah_sepeda_database, name='tambah_sepeda_database'),
	url('delete_sepeda_database/', delete_sepeda_database, name='delete_sepeda_database'),
]
