from django.shortcuts import render
from django.http import HttpResponseRedirect
import psycopg2

# Create your views here.
response = {}
def tambah_sepeda(request):
	return render(request, 'penambahan_sepeda.html', response)
	
def update_sepeda(request):
	return render(request, 'update_sepeda.html', response)
	
def daftar_sepeda(request):
	try:
		connection = psycopg2.connect(
			user = "hvespbbcktcjqh",
			password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
			host = "ec2-54-83-205-27.compute-1.amazonaws.com",
			port = "5432",
			dbname = "d9dmdcj7ao235n"
		)

		cursor = connection.cursor()
		cursor.execute("SELECT * FROM SEPEDA")
		response['data'] = cursor.fetchall()

		connection.commit()
	except (Exception, psycopg2.DatabaseError) as error:
		print ("Error while creating postgreSQL table", error)
	finally:
		if(connection):
			cursor.close()
			connection.close()
	return render(request, 'admin_daftar_sepeda.html', response)

def tambah_sepeda_database(request):
	if(request.method == 'POST'):
		merk = request.POST.get('form16')
		jenis = request.POST.get('form17')
		status = request.POST.get('pilihan_status')
		stasiun = request.POST.get('pilihan_stasiun')
		penyumbang = request.POST.get('pilihan_penyumbang')

		try:
			connection = psycopg2.connect(
				user = "hvespbbcktcjqh",
				password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
				host = "ec2-54-83-205-27.compute-1.amazonaws.com",
				port = "5432",
				dbname = "d9dmdcj7ao235n"
			)

			cursor = connection.cursor()
			cursor.execute("SELECT MAX(nomor) FROM SEPEDA")
			nomor_sepeda = cursor.fetchone()
			print('nomor_sepeda: ', nomor_sepeda)

			cursor.execute("INSERT INTO sepeda VALUES (%s, %s, %s, %s, %s, %s)", (int(nomor_sepeda[0]) + 1,merk, jenis, status, stasiun, penyumbang))
			connection.commit()

		except (Exception, psycopg2.DatabaseError) as error:
			print ("Error while creating postgreSQL table", error)
		
		finally:
			if(connection):
				cursor.close()
				connection.close()

		return HttpResponseRedirect('daftar/')
	else:
		return HttpResponseRedirect('daftar/')

def delete_sepeda_database(request):
	a_id = request.GET.get('id')
	print(a_id)

	try:
		connection = psycopg2.connect(
			user = "hvespbbcktcjqh",
			password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
			host = "ec2-54-83-205-27.compute-1.amazonaws.com",
			port = "5432",
			dbname = "d9dmdcj7ao235n"
		)

		cursor = connection.cursor()
		cursor.execute("DELETE FROM SEPEDA WHERE nomor = %s", (a_id))

		connection.commit()
	except (Exception, psycopg2.DatabaseError) as error:
		print ("Error while creating postgreSQL table", error)
	finally:
		if(connection):
			cursor.close()
			connection.close()
	return HttpResponseRedirect('daftar/')