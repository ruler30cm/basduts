from django.conf.urls import url
from django.urls import path
from .views import daftar_stasiun,update_stasiun,tambah_stasiun, tambah_stasiun_database, delete_stasiun_database

urlpatterns = [
	url('tambah/', tambah_stasiun, name='tambah_stasiun'),
	url('update/', update_stasiun, name='update_stasiun'),
	url('daftar/', daftar_stasiun, name='daftar_stasiun'),
	url('tambah_stasiun_database/', tambah_stasiun_database, name='tambah_stasiun_database'),
	url('delete_stasiun_database/', delete_stasiun_database, name='delete_stasiun_database'),
]
