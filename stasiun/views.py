from django.shortcuts import render
from django.http import HttpResponseRedirect
import psycopg2

# Create your views here.
response = {}
def tambah_stasiun(request):
	return render(request, 'penambahan_stasiun.html', response)
	
def update_stasiun(request):
	return render(request, 'update_stasiun.html', response)
	
def daftar_stasiun(request):
	try:
		connection = psycopg2.connect(
			user = "hvespbbcktcjqh",
			password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
			host = "ec2-54-83-205-27.compute-1.amazonaws.com",
			port = "5432",
			dbname = "d9dmdcj7ao235n"
		)

		cursor = connection.cursor()
		cursor.execute("SELECT * FROM STASIUN")
		response['data'] = cursor.fetchall()

		connection.commit()
	except (Exception, psycopg2.DatabaseError) as error:
		print ("Error while creating postgreSQL table", error)
	finally:
		if(connection):
			cursor.close()
			connection.close()
	return render(request, 'admin_daftar_stasiun.html', response)

def delete_stasiun_database(request):
	a_id = request.GET.get('id')
	print(a_id)

	try:
		connection = psycopg2.connect(
			user = "hvespbbcktcjqh",
			password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
			host = "ec2-54-83-205-27.compute-1.amazonaws.com",
			port = "5432",
			dbname = "d9dmdcj7ao235n"
		)

		cursor = connection.cursor()
		cursor.execute("DELETE FROM STASIUN WHERE id_stasiun = %s", (a_id))

		connection.commit()
	except (Exception, psycopg2.DatabaseError) as error:
		print ("Error while creating postgreSQL table", error)
	finally:
		if(connection):
			cursor.close()
			connection.close()
	return HttpResponseRedirect('daftar/')

def tambah_stasiun_database(request):
	if(request.method == 'POST'):
		nama = request.POST.get('form16')
		alamat = request.POST.get('form17')
		latitude = request.POST.get('form18')
		longitude = request.POST.get('form19')

		try:
			connection = psycopg2.connect(
				user = "hvespbbcktcjqh",
				password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
				host = "ec2-54-83-205-27.compute-1.amazonaws.com",
				port = "5432",
				dbname = "d9dmdcj7ao235n"
			)

			cursor = connection.cursor()
			cursor.execute("SELECT MAX(id_stasiun) FROM STASIUN")
			nomor_stasiun = cursor.fetchone()
			print('nomor_stasiun: ', nomor_stasiun)

			cursor.execute("INSERT INTO STASIUN VALUES (%s, %s, %s, %s, %s)", (int(nomor_stasiun[0]) + 1, alamat, latitude, longitude, nama))
			connection.commit()

		except (Exception, psycopg2.DatabaseError) as error:
			print ("Error while creating postgreSQL table", error)
		
		finally:
			if(connection):
				cursor.close()
				connection.close()

		return HttpResponseRedirect('daftar/')
	else:
		return HttpResponseRedirect('daftar/')
