from django.shortcuts import render, redirect
import psycopg2
from django.contrib import messages
from django.contrib.auth import logout

# Create your views here.
response = {}
def index(request):
	if request.method == "POST":
		no_ktp = request.POST.get("no_ktp")
		nama_lengkap = request.POST.get("nama_lengkap")
		email = request.POST.get("email")
		tanggal_lahir = request.POST.get("tanggal_lahir")
		nomor_telepon = request.POST.get("nomor_telepon")
		alamat = request.POST.get("alamat")
		role = request.POST.get("role")

		try:
			connection = psycopg2.connect(
        		user = "hvespbbcktcjqh",
        		password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
            	host = "ec2-54-83-205-27.compute-1.amazonaws.com",
            	port = "5432",
            	dbname = "d9dmdcj7ao235n"
        	)

			cursor = connection.cursor()
		
			cursor.execute("SELECT ktp, email FROM PERSON WHERE ktp = %s OR email = %s;", (no_ktp, email)) # cek apakah ada no ktp ato email yg sama
			exists = cursor.fetchone()
			if exists is not None:
				if str(exists[0]) == str(no_ktp) or str(exists[1]) == str(email):
					messages.error(request,'no_ktp or email already exists')
					return redirect('/register/')
			
			if tanggal_lahir == "" and nomor_telepon == "": # kalau tanggal lahir sama nomor telepon tidak ada
				cursor.execute("INSERT INTO PERSON (ktp, email, nama, tgl_lahir) VALUES (%s, %s, %s, %s);", (no_ktp, email, nama_lengkap, tanggal_lahir))
			else:
				cursor.execute("INSERT INTO PERSON VALUES (%s, %s, %s, %s, %s, %s);", (no_ktp, email, nama_lengkap, alamat, tanggal_lahir, nomor_telepon))
			
			if role == 'petugas': # buat mengisi role nya masing-masing
				cursor.execute("INSERT INTO PETUGAS VALUES (%s, %s);", (no_ktp, 30000))
			elif role == 'anggota':
				cursor.execute("SELECT MAX(no_kartu) FROM ANGGOTA;")
				no_kartu = cursor.fetchone()
				cursor.execute("INSERT INTO ANGGOTA VALUES (%s, %s, %s, %s);", (int(no_kartu[0]) + 1, 0, 0, no_ktp))

			request.session['no_ktp'] = no_ktp # menyimpan di session
			request.session['email'] = email
			request.session['role'] = role

			connection.commit()

			if role == 'petugas':
				return redirect('/penugasan/daftar/')
			elif role == 'anggota':
				return redirect('/sepeda/daftar')
		except (Exception, psycopg2.DatabaseError) as error:
			print("Error while executing query: ", error)
		finally:
			if(connection):
				cursor.close()
				connection.close()
				print("Successfully created query")
	return render(request, 'home_register.html', response)
def index_login(request):
	if request.method == "POST":
		no_ktp = request.POST.get('no_ktp')
		email = request.POST.get('email')

		try:
			connection = psycopg2.connect(
        		user = "hvespbbcktcjqh",
        		password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
            	host = "ec2-54-83-205-27.compute-1.amazonaws.com",
            	port = "5432",
            	dbname = "d9dmdcj7ao235n"
        	)

			cursor = connection.cursor()

			cursor.execute("SELECT ktp, email FROM PERSON WHERE ktp = %s OR email = %s;", (no_ktp, email))
			exists = cursor.fetchone()
			if exists is None:
				messages.error(request,'no_ktp or email does NOT exists')
				return redirect('/register/')
			
			cursor.execute("SELECT DISTINCT P.email, A.ktp FROM PERSON P JOIN ANGGOTA A ON A.ktp = p.ktp WHERE A.ktp = %s AND P.email = %s;", (no_ktp, email))
			exists = cursor.fetchone()
			if exists is not None:
				request.session['role'] = 'anggota'
				request.session['no_ktp'] = no_ktp
				request.session['email'] = email
				return redirect('/sepeda/daftar/')
			
			cursor.execute("SELECT DISTINCT P.ktp, PE.email FROM PERSON PE JOIN PETUGAS P ON P.ktp = PE.ktp WHERE P.ktp = %s AND PE.email = %s;", (no_ktp, email))
			exists = cursor.fetchone()
			if exists is not None:
				request.session['role'] = 'petugas'
				request.session['no_ktp'] = no_ktp
				request.session['email'] = email
				return redirect('/penugasan/daftar/')
		except (Exception, psycopg2.DatabaseError) as error:
			print ("Error while executing query: ", error)
		finally:
			if(connection):
				cursor.close()
				connection.close()
				print("Successfully created query, login: ")
			
	return render(request, 'home_login.html', response)

def logout_func(request):
	logout(request)
	return redirect('/login/')