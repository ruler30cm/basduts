from django.conf.urls import url
from django.urls import path
from .views import index, index_login, logout_func

urlpatterns = [
	url('register/', index, name='index'),
	url('login/', index_login, name='index_login'),
	url('logout/', logout_func, name='logout_func')
]
