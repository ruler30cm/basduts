from django.shortcuts import render
from django.http import HttpResponseRedirect
import psycopg2
# Create your views here.
response = {}
def tambah_voucher(request):
	return render(request, 'penambahan_voucher.html', response)
	
def update_voucher(request):
	return render(request, 'update_voucher.html', response)
	
def daftar_voucher(request):
	try:
		connection = psycopg2.connect(
			user = "hvespbbcktcjqh",
			password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
			host = "ec2-54-83-205-27.compute-1.amazonaws.com",
			port = "5432",
			dbname = "d9dmdcj7ao235n"
		)

		cursor = connection.cursor()
		cursor.execute("SELECT * FROM VOUCHER")
		response['data'] = cursor.fetchall()

		connection.commit()
	except (Exception, psycopg2.DatabaseError) as error:
		print ("Error while creating postgreSQL table", error)
	finally:
		if(connection):
			cursor.close()
			connection.close()
	return render(request, 'admin_daftar_voucher.html', response)

def delete_voucher_database(request):
	a_id = request.GET.get('id')
	print(a_id)

	try:
		connection = psycopg2.connect(
			user = "hvespbbcktcjqh",
			password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
			host = "ec2-54-83-205-27.compute-1.amazonaws.com",
			port = "5432",
			dbname = "d9dmdcj7ao235n"
		)

		cursor = connection.cursor()
		cursor.execute("DELETE FROM VOUCHER WHERE id_voucher = %s", (a_id))

		connection.commit()
	except (Exception, psycopg2.DatabaseError) as error:
		print ("Error while creating postgreSQL table", error)
	finally:
		if(connection):
			cursor.close()
			connection.close()
	return HttpResponseRedirect('daftar/')

def tambah_voucher_database(request):
	if(request.method == 'POST'):
		nama = request.POST.get('nama')
		kategori = request.POST.get('kategori')
		poin = request.POST.get('poin')
		deskripsi = request.POST.get('deskripsi')
		jumlah = request.POST.get('jumlah')

		try:
			connection = psycopg2.connect(
				user = "hvespbbcktcjqh",
				password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
				host = "ec2-54-83-205-27.compute-1.amazonaws.com",
				port = "5432",
				dbname = "d9dmdcj7ao235n"
			)

			cursor = connection.cursor()

			for i in range(int (jumlah)):
				cursor.execute("SELECT MAX(id_voucher) FROM VOUCHER")
				nomor_voucher = cursor.fetchone()
				print('nomor_voucher: ', nomor_voucher)
				cursor.execute("INSERT INTO voucher VALUES (%s, %s, %s, %s, %s, %s)", (int(nomor_voucher[0])+1,nama, kategori, poin, deskripsi, 1))
				connection.commit()

		except (Exception, psycopg2.DatabaseError) as error:
			print ("Error while creating postgreSQL table", error)
		
		finally:
			if(connection):
				cursor.close()
				connection.close()

		return HttpResponseRedirect('daftar/')
	else:
		return HttpResponseRedirect('daftar/')

def update_voucher_database(request): 

	if(request.method == 'POST'):
		id_voucher = request.POST.get('id_voucher')
		nama = request.POST.get('nama')
		kategori = request.POST.get('kategori')
		poin = request.POST.get('poin')
		deskripsi = request.POST.get('deskripsi')

		try:
			connection = psycopg2.connect(
				user = "hvespbbcktcjqh",
				password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
				host = "ec2-54-83-205-27.compute-1.amazonaws.com",
				port = "5432",
				dbname = "d9dmdcj7ao235n"
			)

			cursor = connection.cursor()

			cursor.execute("UPDATE VOUCHER SET nama = %s, kategori = %s, nilai_poin = %s, deskripsi = %s WHERE id_voucher = %s", (nama, kategori, poin, deskripsi, id_voucher))
			
			connection.commit()

		except (Exception, psycopg2.DatabaseError) as error:
			print ("Error while creating postgreSQL table", error)
		
		finally:
			if(connection):
				cursor.close()
				connection.close()

		return HttpResponseRedirect('daftar/')
	else:
		return HttpResponseRedirect('daftar/')

