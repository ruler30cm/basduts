from django.conf.urls import url
from django.urls import path
from .views import daftar_voucher,update_voucher,tambah_voucher,tambah_voucher_database, delete_voucher_database, update_voucher_database

urlpatterns = [
	url('tambah/', tambah_voucher, name='tambah_voucher'),
	url('update/', update_voucher, name='update_voucher'),
	url('daftar/', daftar_voucher, name='daftar_voucher'),
	url('delete_voucher_database/', delete_voucher_database, name='delete_voucher_database'),
	url('tambah_voucher_database/', tambah_voucher_database, name='tambah_voucher_database'),
	url('update_voucher_database/', update_voucher_database, name='update_voucher_database'),
]
