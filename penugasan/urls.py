from django.conf.urls import url
from django.urls import path
from .views import daftar_penugasan,update_penugasan, tambah_penugasan, tambah_penugasan_database, delete_penugasan_database,update_penugasan_database

urlpatterns = [
	url('tambah/', tambah_penugasan, name='tambah_penugasan'),
	url('update/', update_penugasan, name='update_penugasan'),
	url('daftar/', daftar_penugasan, name='daftar_penugasan'),
	url('update_penugasan_database/', update_penugasan_database, name='update_penugasan_database'),
	url('tambah_penugasan_database/', tambah_penugasan_database, name='tambah_penugasan_database'),
	url('delete_penugasan_database/', delete_penugasan_database, name='delete_penugasan_database'),
]
