from django.shortcuts import render
from django.http import HttpResponseRedirect
import psycopg2

# Create your views here.
response = {}
def tambah_penugasan(request):
	try:
		connection = psycopg2.connect(
			user = "hvespbbcktcjqh",
			password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
			host = "ec2-54-83-205-27.compute-1.amazonaws.com",
			port = "5432",
			dbname = "d9dmdcj7ao235n"
		)

		cursor = connection.cursor()
		cursor.execute("SELECT A.ktp, nama FROM PETUGAS A,PERSON B WHERE A.ktp = B.ktp")
		response['data_petugas'] = cursor.fetchall()

		connection.commit()
	except (Exception, psycopg2.DatabaseError) as error:
		print ("Error while creating postgreSQL table", error)
	finally:
		if(connection):
			cursor.close()
			connection.close()
	return render(request, 'form_penugasan.html', response)
	
def update_penugasan(request):
	return render(request, 'update_penugasan.html', response)
	
def daftar_penugasan(request):
	try:
		connection = psycopg2.connect(
			user = "hvespbbcktcjqh",
			password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
			host = "ec2-54-83-205-27.compute-1.amazonaws.com",
			port = "5432",
			dbname = "d9dmdcj7ao235n"
		)

		cursor = connection.cursor()
		cursor.execute("SELECT P.ktp, P.nama, start_datetime, end_datetime, S.nama FROM PENUGASAN PN, PERSON P, STASIUN S WHERE PN.ktp = P.ktp AND PN.id_stasiun = S.id_stasiun")
		response['data'] = cursor.fetchall()

		cursor.execute("SELECT A.ktp,nama FROM PETUGAS A,PERSON B WHERE A.ktp = B.ktp")
		response['data_petugas'] = cursor.fetchall()

		cursor.execute("SELECT P.id_stasiun, nama FROM STASIUN S, PENUGASAN P WHERE S.id_stasiun = P.id_stasiun")
		response['data_stasiun'] = cursor.fetchall()

		connection.commit()
	except (Exception, psycopg2.DatabaseError) as error:
		print ("Error while creating postgreSQL table", error)
	finally:
		if(connection):
			cursor.close()
			connection.close()
	return render(request, 'tabel_penugasan.html', response)

def tambah_penugasan_database(request):
	if(request.method == 'POST'):
		petugas = request.POST.get('petugas')
		mulai = request.POST.get('mulai')
		selesai = request.POST.get('selesai')
		stasiun = request.POST.get('stasiun')

		try:
			connection = psycopg2.connect(
				user = "hvespbbcktcjqh",
				password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
				host = "ec2-54-83-205-27.compute-1.amazonaws.com",
				port = "5432",
				dbname = "d9dmdcj7ao235n"
			)

			cursor = connection.cursor()
			cursor.execute("INSERT INTO PENUGASAN VALUES (%s, %s, %s, %s)", (petugas, mulai, stasiun, selesai))
			connection.commit()

		except (Exception, psycopg2.DatabaseError) as error:
			print ("Error while creating postgreSQL table", error)
		
		finally:
			if(connection):
				cursor.close()
				connection.close()

		return HttpResponseRedirect('daftar/')
	else:
		return HttpResponseRedirect('daftar/')
	
def delete_penugasan_database(request):
	ktp = request.GET.get('id')
	new_ktp = ktp.split('-')
	
	try:
		connection = psycopg2.connect(
			user = "hvespbbcktcjqh",
			password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
			host = "ec2-54-83-205-27.compute-1.amazonaws.com",
			port = "5432",
			dbname = "d9dmdcj7ao235n"
		)

		cursor = connection.cursor()
		cursor.execute("DELETE FROM PENUGASAN WHERE ktp = %s", [new_ktp[0]])

		connection.commit()
	except (Exception, psycopg2.DatabaseError) as error:
		print ("Error while creating postgreSQL table", error)
	finally:
		if(connection):
			cursor.close()
			connection.close()
	return HttpResponseRedirect('daftar/')

def update_penugasan_database(request): 

	if(request.method == 'POST'):
		petugas = request.POST.get('petugas')
		mulai = request.POST.get('mulai')
		selesai = request.POST.get('selesai')
		stasiun = request.POST.get('stasiun')

		new_petugas = petugas.split(" - ")

		try:
			connection = psycopg2.connect(
				user = "hvespbbcktcjqh",
				password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
				host = "ec2-54-83-205-27.compute-1.amazonaws.com",
				port = "5432",
				dbname = "d9dmdcj7ao235n"
			)

			cursor = connection.cursor()

			cursor.execute("UPDATE PENUGASAN SET ktp = %s, start_datetime = %s, end_datetime = %s, id_stasiun = %s WHERE ktp = %s", (new_petugas[0], mulai, selesai, stasiun, new_petugas[0]))
			
			connection.commit()

		except (Exception, psycopg2.DatabaseError) as error:
			print ("Error while creating postgreSQL table", error)
		
		finally:
			if(connection):
				cursor.close()
				connection.close()

		return HttpResponseRedirect('daftar/')
	else:
		return HttpResponseRedirect('daftar/')