from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import psycopg2
from django.http import HttpResponseForbidden

# Create your views here.
response = {}
def daftar_laporan(request):
	if 'no_ktp' in request.session.keys():
		if request.session['role'] == 'petugas':
			no_ktp = request.session["no_ktp"]
			email = request.session['email']
			page = request.GET.get('page', 1)
			try:
				connection = psycopg2.connect(
					user = "hvespbbcktcjqh",
					password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
					host = "ec2-54-83-205-27.compute-1.amazonaws.com",
					port = "5432",
					dbname = "d9dmdcj7ao235n"
				)
				cursor = connection.cursor()
				sorts = request.GET.get('sort')
				if sorts == '0':
					sql = "SELECT DISTINCT L.id_laporan, L.datetime_pinjam, L.no_kartu_anggota, P.nama, PE.denda, L.status FROM LAPORAN L NATURAL JOIN PEMINJAMAN PE, PERSON P JOIN ANGGOTA A ON A.ktp = P.ktp ORDER BY L.id_laporan;"
					cursor.execute(sql)
				elif sorts == '1':
					sql = "SELECT DISTINCT L.id_laporan, L.datetime_pinjam, L.no_kartu_anggota, P.nama, PE.denda, L.status FROM LAPORAN L NATURAL JOIN PEMINJAMAN PE, PERSON P JOIN ANGGOTA A ON A.ktp = P.ktp ORDER BY L.datetime_pinjam;"
					cursor.execute(sql)
				elif sorts == '4 ASC':
					sql = "SELECT DISTINCT L.id_laporan, L.datetime_pinjam, L.no_kartu_anggota, P.nama, PE.denda, L.status FROM LAPORAN L NATURAL JOIN PEMINJAMAN PE, PERSON P JOIN ANGGOTA A ON A.ktp = P.ktp ORDER BY PE.denda ASC;"
					cursor.execute(sql)
				elif sorts == '4 DESC':
					sql = "SELECT DISTINCT L.id_laporan, L.datetime_pinjam, L.no_kartu_anggota, P.nama, PE.denda, L.status FROM LAPORAN L NATURAL JOIN PEMINJAMAN PE, PERSON P JOIN ANGGOTA A ON A.ktp = P.ktp ORDER BY PE.denda DESC;"
					cursor.execute(sql)
				elif sorts == '5':
					sql = "SELECT DISTINCT L.id_laporan, L.datetime_pinjam, L.no_kartu_anggota, P.nama, PE.denda, L.status FROM LAPORAN L NATURAL JOIN PEMINJAMAN PE, PERSON P JOIN ANGGOTA A ON A.ktp = P.ktp ORDER BY L.status;"
					cursor.execute(sql)
				else:
					cursor.execute("SELECT DISTINCT L.id_laporan, L.datetime_pinjam, L.no_kartu_anggota, P.nama, PE.denda, L.status FROM LAPORAN L NATURAL JOIN PEMINJAMAN PE, PERSON P JOIN ANGGOTA A ON A.ktp = P.ktp WHERE PE.no_kartu_anggota = A.no_kartu;")
				laporan = cursor.fetchall()
				if laporan is not None:
					paginator = Paginator(laporan, 10)
					try:
						laporans = paginator.page(page)
					except PageNotAnInteger:
						laporans = paginator.page(1)
					except EmptyPage:
						laporans = paginator.page(paginator.num_pages)
					response['data'] = laporans
			except (Exception, psycopg2.DatabaseError) as error:
				print ("Error while executing query: ", error)
			finally:
				if (connection):
					cursor.close()
					connection.close()
					print("Successfully created query")
		else:
			return HttpResponseForbidden("Forbidden")
	else:
		return redirect('/login/')
	return render(request, 'admin_daftar_laporan.html', response)
	

