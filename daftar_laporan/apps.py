from django.apps import AppConfig


class DaftarLaporanConfig(AppConfig):
    name = 'daftar_laporan'
