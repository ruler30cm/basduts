from django.shortcuts import render, redirect
from django.http import HttpResponseForbidden
from datetime import datetime
import psycopg2
# Create your views here.
response = {}
def daftar_transaksi(request):
	if 'no_ktp' in request.session.keys():
		if request.session['role'] == 'anggota':
			no_ktp = request.session["no_ktp"]
			email = request.session['email']
			try:
				connection = psycopg2.connect(
					user = "hvespbbcktcjqh",
					password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
					host = "ec2-54-83-205-27.compute-1.amazonaws.com",
					port = "5432",
					dbname = "d9dmdcj7ao235n"
				)
						
				cursor = connection.cursor()
				cursor.execute("SELECT A.no_kartu FROM ANGGOTA A JOIN PERSON P ON P.ktp = A.ktp WHERE P.ktp = %s and P.email = %s;", (no_ktp, email))
				no_kartu = cursor.fetchone()[0]

				cursor.execute("SELECT date_time, jenis, nominal FROM TRANSAKSI WHERE no_kartu_anggota = %s;", (no_kartu))
				response['data'] = cursor.fetchall()
			except (Exception, psycopg2.DatabaseError) as error:
				print ("Error while executing query: ", error)
			finally:
				if(connection):
					cursor.close()
					connection.close()
					print("Successfully created query,")
		else:
			return HttpResponseForbidden("Forbidden")
	else:
		return redirect('/login/')
	return render(request, 'daftar_transaksi.html', response)

def tambah_transaksi(request):
	if 'no_ktp' in request.session.keys():
		if request.session['role'] == 'anggota':
			if request.method == "GET":
				nominal = request.GET.get('nominal')
				no_ktp = request.session['no_ktp']
				email = request.session['email']
				if nominal is not None:
					if int(nominal) > 0:
						try:
							connection = psycopg2.connect(
								user = "hvespbbcktcjqh",
								password = "951b0d2ed556792121cfb56923d88033b74110aa2228c019062a071d85ac3615",
								host = "ec2-54-83-205-27.compute-1.amazonaws.com",
								port = "5432",
								dbname = "d9dmdcj7ao235n"
							)
							
							cursor = connection.cursor()
							cursor.execute("SELECT A.from django.http import HttpResponseForbiddenno_kartu FROM ANGGOTA A JOIN PERSON P ON P.ktp = A.ktp WHERE P.ktp = %s and P.email = %s;", (no_ktp, email))
							no_kartu = cursor.fetchone()[0]

							cursor.execute("INSERT INTO TRANSAKSI VALUES (%s, %s, %s, %s);", (no_kartu, datetime.now(), "Topup", nominal))
							cursor.execute("UPDATE ANGGOTA SET SALDO = SALDO + %s WHERE ktp = %s AND no_kartu = %s;", (nominal, no_ktp, no_kartu))
							connection.commit()
						except (Exception, psycopg2.DatabaseError) as error:
							print ("Error while executing query: ", error)
						finally:
							if(connection):
								cursor.close()
								connection.close()
								print("Successfully created query")
								return redirect('/transaksi/daftar/')
					else:
						return redirect('/transaksi/tambah/')
		else:
			return HttpResponseForbidden("Forbidden")
	else:
		return redirect('/login/')
	return render(request, 'create_transaksi.html', response)