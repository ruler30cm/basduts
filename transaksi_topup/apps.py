from django.apps import AppConfig


class TransaksiTopupConfig(AppConfig):
    name = 'transaksi_topup'
