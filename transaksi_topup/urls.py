from django.conf.urls import url
from django.urls import path
from .views import daftar_transaksi,tambah_transaksi

urlpatterns = [
	url('tambah/', tambah_transaksi, name='tambah_transaksi'),
	url('daftar/', daftar_transaksi, name='daftar_transaksi'),
]
